
import os
import sys
from setuptools import setup, find_packages

PY_VER = sys.version_info

if PY_VER >= (3, 5):
    pass
elif PY_VER >= (3, 4):
    raise RuntimeError("You need python3.5 or newer")

setup(
    name='trantor-ebookdl',
    version='0.1',
    license='AGPL3',
    author='David Ferlier',
    description='Ebooks downloader for the Imperial Library',
    packages=[],
    include_package_data=False,
    platforms='linux',
    scripts=['trantor-ebookdl'],
    install_requires=[
        'aiohttp>=2.0.0',
        'aiosocks>=0.2.0',
        ],
    classifiers=[
        'Programming Language :: Python',
        'Development Status :: 4 - Beta',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'License :: OSI Approved :: GNU Affero General Public License v3',
        'Topic :: Software Development :: User Interfaces'
    ],
)
